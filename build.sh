#!/bin/sh
#
# Install script for Thanos
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="ca-certificates curl dpkg-dev"

# Packages to install.
PACKAGES=""

THANOS_RELEASE=0.23.0

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

THANOS_RELEASE_URL="https://github.com/thanos-io/thanos/releases/download/v${THANOS_RELEASE}/thanos-${THANOS_RELEASE}.linux-$(dpkg-architecture -q DEB_HOST_ARCH).tar.gz"
curl -sL ${THANOS_RELEASE_URL} \
    | tar --wildcards -xOzvf - '*thanos' \
    > /usr/sbin/thanos
chmod 755 /usr/sbin/thanos

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
