docker-thanos
===

A Docker image to run Thanos. All Thanos components are run by default on
container startup.

Individual components can be disabled by setting `<COMPONENT>_OFF` environment variables.

Component ports are configured as described here:
https://thanos.io/getting-started.md/#testing-thanos-on-single-host

The following environment variables are understood:

```
QUERY_OFF # true/yes
QUERY_HTTP_ADDRESS
QUERY_GRPC_ADDRESS
QUERY_FLAGS

SIDECAR_OFF # true/yes
SIDECAR_HTTP_ADDRESS
SIDECAR_GRPC_ADDRESS
SIDECAR_FLAGS
```