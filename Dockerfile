FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

COPY conf /etc/
COPY build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

